-- Coal in slot 1 (for refuel)
-- Chests in slot 2 (for emptying inv)
-- Torches in slot 3 (for light obv)
-- Cobblestone in slot 4 (for filling gaps)

local distance = 0
local amountTunnels = 1
local goLeft = 0
local placeTorches = 1

local betweenTunnels = 2
local minimumFuel = 50

local currentTunnel = 0
local stop = false
local outOfTorches = false
local inventoryFull = false


local function checkStatus()
  -- check if it has fuel 
  if (turtle.getItemCount(1) == 0) then
    stop = true
  end

  -- check if it has chests
  if (turtle.getItemCount(2) == 0) then
    stop = true
  end

  -- check if it has torches
  if (placeTorches == 1) and (turtle.getItemCount(3) == 0) then
    outOfTorches = true
  else
    outOfTorches = false
  end

  -- check if inventory is full
  if (turtle.getItemCount(16) > 0) then
    inventoryFull = true
  end

  -- check if it's done digging
  if (currentTunnel == amountTunnels) then
    stop = true
  end

  -- check and refill if it's starting to run out of fuel
  while (turtle.getFuelLevel() < minimumFuel) do
    turtle.select(1)
    turtle.refuel(1)
  end
end

local function digForward()
  -- dig for as long as there are blocks in front (loop in case of gravel)
  while (turtle.detect()) do
    turtle.dig()
    sleep(0.5)
  end
end

local function emptyInventory()
  -- make space in tunnel wall for chest
  turtle.turnLeft()
  digForward()

  turtle.forward()

  if (turtle.detectUp()) then
    turtle.digUp()
  end

  -- go back into tunnel
  turtle.back()

  -- place chest
  turtle.select(2)
  turtle.place()

  -- empty inventory into chest
  for slot = 5,16 do
    turtle.select(slot)
    turtle.drop()
    sleep(0.5)
  end

  -- turn back to continue digging
  turtle.turnRight()

  -- reset inventory full var
  inventoryFull = false

  turtle.select(1)
end

-- direction: 0 = right, 1 = left, 2 = front (for ends of tunnels)
local function placeTorch(direction)
  if (direction == 0) then
    -- go one back to already dug spot
    turtle.back()

    -- make sure there is block to place torch on, if not place block
    turtle.up()
    turtle.turnLeft()
  
    if not (turtle.detect()) then
      turtle.select(4)
      turtle.place()
    end
  
    -- place torch
    turtle.turnRight()
    turtle.down()
    turtle.select(3)
    turtle.placeUp()
  
    -- go one forward to resume digging
    turtle.forward()
  elseif (direction == 1) then
    -- go one back to already dug spot
    turtle.back()

    -- make sure there is block to place torch on, if not place block
    turtle.up()
    turtle.turnRight()

    if not (turtle.detect()) then
      turtle.select(4)
      turtle.place()
    end

    -- place torch
    turtle.turnLeft()
    turtle.down()
    turtle.select(3)
    turtle.placeUp()

    -- go one forward to resume digging
    turtle.forward()
  else
    -- make sure there is block to place torch on, if not place block
    turtle.up()
    turtle.turnLeft()
    turtle.dig()
    turtle.turnLeft()
    turtle.dig()
    turtle.turnLeft()
    turtle.dig()
    turtle.turnLeft()

    if not (turtle.detect()) then
      turtle.select(4)
      turtle.place()
    end

    -- place torch
    turtle.down()
    turtle.select(3)
    turtle.placeUp()
  end
end

local function run()
  -- keeps current tunnel distance
  local currentDistance = 0

  -- digging loop
  while not (stop) do
    -- check status of turtle for each loop
    checkStatus()


    if (inventoryFull) then -- empty inventory if full
      emptyInventory()
    elseif (outOfTorches) then -- stop and wait for torches if ran out
      print("Ran out of torches, will pause until refilled. Enter anything to continue")
      io.read()
    else
      -- check if it's done digging tunnel
      if (currentDistance ~= distance) then

        -- place torch
        if ((placeTorches == 1) and (currentDistance ~= 0) and (currentDistance % 11 == 0)) then
          placeTorch(0)
        end

        -- dig one forward
        digForward()

        -- move forward
        if (turtle.forward()) then
          currentDistance = currentDistance + 1
        end

        -- dig up for 2 high tunnels
        if (turtle.detectUp()) then
          turtle.digUp()
        end
    
        -- place block below if missing (for nicer and easier to traverse tunnels)
        if not (turtle.detectDown()) then
          turtle.select(4)
          turtle.placeDown()
          turtle.select(1)
        end
      else
        -- place torch at end
        placeTorch(2)

        -- turn around
        turtle.turnLeft()
        turtle.turnLeft()

        -- move back to start of tunnel
        for i=1,currentDistance do
          if (turtle.detect()) then
            turtle.dig()
          end
          turtle.forward()
        end

        -- place torch
        placeTorch(2)

        -- turn either left or right for new tunnel
        if (goLeft == 1) then
          turtle.turnRight()
        else
          turtle.turnLeft()
        end

        -- dig over to new tunnel start
        for i=1,(betweenTunnels+1) do
          digForward()
          turtle.forward()
          if turtle.detectUp() then
            turtle.digUp()
          end
        end

        -- turn towards new tunnel
        if (goLeft == 1) then
          turtle.turnRight()
        else
          turtle.turnLeft()
        end

        -- reset current distance and increment current tunnel
        currentDistance = 0
        currentTunnel = currentTunnel + 1
      end
    end
  end
end


-- ask user for inputs
print("Mining turtle cool yeah")
print("How deep tunnels? (int)")
distance = tonumber(io.read())

print("How many tunnels? (int)")
amountTunnels = tonumber(io.read())

print("0 = Right, 1 = Left (int)")
goLeft = tonumber(io.read())

print("Should the turtle place torches? 0 = No, 1 = Yes. If yes it will stop mining if it runs out of torches and wait to be refilled (int)")
placeTorches = tonumber(io.read())

print("Turtle will auto-refuel and place chests to empty it's inventory. If it runs out of chests or coal it will shut down")
print("Put coal in slot 1, chests in slot 2, torches in slot 3 and cobblestone in slot 4. Enter anything to continue :-)")
io.read()

-- start digging loop
run()